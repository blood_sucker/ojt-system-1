package com.example.ui;

import java.util.Date;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class CoopUI extends VerticalLayout
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HorizontalLayout horizontalL;
	
	private TextField clamantName = new TextField();
	private TextField insuredName = new TextField();
	private DateField dateOfBirth = new DateField();
	private TextField age = new TextField();
	private DateField dateFrom = new DateField();
	private DateField dateTo = new DateField();
	private TextField numberDay = new TextField();
	private TextArea cause = new TextArea();
	
	public CoopUI ()
	{
		
        setWidth("100%");
        setSpacing(true);
        setStyleName("coop-info");
        
        
        addComponent(clamantName);
        clamantName.setWidth("100%");
        clamantName.setCaption("Claimant name");
        clamantName.setStyleName("claimant-name");
        
        addComponent(insuredName);
        insuredName.setWidth("100%");
        insuredName.setCaption("Insured name");
        insuredName.setStyleName("insured-name");
        
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setWidth("100%");
        horizontalL.setSpacing(true);
        
        horizontalL.addComponent(dateOfBirth);
        dateOfBirth.setWidth("100%");
        dateOfBirth.setCaption("Date of Birth");
        dateOfBirth.setStyleName("date-birth");
        
        horizontalL.addComponent(age);
        age.setWidth("100%");
        age.setCaption("Age");
        age.setStyleName("age");
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");
        
        
        horizontalL.addComponent(dateFrom);
        dateFrom.setWidth("100%");
        dateFrom.setCaption("Date from");
        dateFrom.setStyleName("date-from");
        
        horizontalL.addComponent(dateTo);
        dateTo.setWidth("100%");
        dateTo.setCaption("Date to");
        dateTo.setStyleName("date-to");
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");
        
        horizontalL.addComponent(numberDay);
        horizontalL.setComponentAlignment(numberDay, Alignment.MIDDLE_LEFT);
        numberDay.setCaption("No. days");
        numberDay.setWidth("100%");
        numberDay.setStyleName("number-days");


        horizontalL.addComponent(cause);
        cause.setWidth("100%");
        cause.setCaption("Cause");
        cause.setStyleName("cause");
	}

	public String getClamantName() {
		return clamantName.getValue();
	}

	public String getInsuredName() {
		return insuredName.getValue();
	}

	public Date getDateOfBirth() {
		return dateOfBirth.getValue();
	}

	public String getAge() {
		return age.getValue();
	}

	public Date getDateFrom() {
		return dateFrom.getValue();
	}

	public Date getDateTo() {
		return dateTo.getValue();
	}

	public String getNumberDay() {
		return numberDay.getValue();
	}

	public String getCause() {
		return cause.getValue();
	}

	public void setClamantName(String clamantName) {
		this.clamantName.setValue(clamantName);
	}

	public void setInsuredName(String insuredName) {
		this.insuredName.setValue(insuredName);
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth.setValue(dateOfBirth);
	}

	public void setAge(String age) {
		this.age.setValue(age);
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom.setValue(dateFrom);
	}

	public void setDateTo(Date dateTo) {
		this.dateTo.setValue(dateTo);
	}

	public void setNumberDay(String numberDay) {
		this.numberDay.setValue(numberDay);
	}

	public void setCause(String cause) {
		this.cause.setValue(cause);
	}
}
