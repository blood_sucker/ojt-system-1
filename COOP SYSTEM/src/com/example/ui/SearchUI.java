package com.example.ui;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.coop_system.Coop_systemUI;
import com.example.model.GroupClient;
import com.example.model.Product;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class SearchUI extends CssLayout
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HorizontalLayout horizontalL;
	private VerticalLayout vLayout;
	
	private DBQuery select = new DBQuery(DBConnection.getConnection());
	
	private TextField seachField = new TextField();
	private CheckBox interactive = new CheckBox();
	private ComboBox coopN = new ComboBox();
	private ComboBox prod = new ComboBox();
	private ComboBox prodStatus = new ComboBox();
	
	public SearchUI ()
	{
		
		setWidth("100%");
        setStyleName("search-bar");
        
        vLayout = new VerticalLayout();
        addComponent(vLayout);
        vLayout.setSpacing(true);
        vLayout.setWidth("100%");
        
        horizontalL = new HorizontalLayout();
        vLayout.addComponent(horizontalL);
        horizontalL.setWidth("100%");
        horizontalL.setStyleName("search-horizon");
        
        horizontalL.addComponent(seachField);
        seachField.setWidth("100%");
        seachField.setInputPrompt("Search claimant...");
        seachField.setStyleName("search-box");
        seachField.setTextChangeEventMode(TextChangeEventMode.EAGER);
        seachField.setImmediate(true);
        
        Button searchBtn = new Button();
        horizontalL.addComponent(searchBtn);
        searchBtn.setCaption("Search");
        searchBtn.setIcon(FontAwesome.SEARCH);
        searchBtn.setStyleName("submit-search");
        searchBtn.setDescription("Click to Search");
        
        vLayout.addComponent(interactive);
        interactive.setCaption("Interactive searching ?");
        interactive.setStyleName("interactive");
        interactive.setValue(true);
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");
        
        horizontalL.addComponent(coopN);
        coopN.setWidth("100%");
        coopN.setCaption("Coop name");
        coopN.setStyleName("s-coop-name");
        ResultSet listCoopName = select.select(GroupClient.getGroupClient("CoopNum, Name"));
        try
        {
        	
        	while(listCoopName.next())
        	{
        		
        		coopN.addItem(listCoopName.getString("CoopNum"));
        		coopN.setItemCaption(listCoopName.getString("CoopNum"), listCoopName.getString("Name") );
        	}
        	
        	select.closeResult();
        	select.closeStatement();
        	listCoopName.close();
        } catch (SQLException e)
        {
        	e.printStackTrace();
        }
        
        horizontalL.addComponent(prod);
        prod.setWidth("100%");
        prod.setCaption("Product");
        prod.setStyleName("s-product");
        ResultSet listProduct = select.select(Product.getProduct("Product_no, ProductLongName"));
        
        try
        {
        	
        	while(listProduct.next())
        	{
        		
        		prod.addItem(listProduct.getString("Product_no"));
        		prod.setItemCaption(listProduct.getString("Product_no"), listProduct.getString("ProductLongName"));
        	}
        	select.closeStatement();
        	select.closeStatement();
        	listProduct.close();
        } catch (SQLException e)
        {
        	e.printStackTrace();
        }
        
        
        horizontalL.addComponent(prodStatus);
        prodStatus.setCaption("Product Status");
        prodStatus.setWidth("100%");
        prodStatus.setStyleName("prod-status");
        prodStatus.setNullSelectionAllowed(false);
        prodStatus.addItem(0);
        prodStatus.addItem(1);
        prodStatus.addItem("");
        prodStatus.setItemCaption(0, "UNPAID");
        prodStatus.setItemCaption(1, "PAID");
        prodStatus.setItemCaption("", "BOTH");
        prodStatus.setValue("");
        
        
        seachField.addTextChangeListener(new TextChangeListener() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void textChange(TextChangeEvent event) {
				// TODO Auto-generated method stub
				
				String search = event.getText().trim();
				
				if( interactive.getValue())
				{
					
					if( search.length() > 3 )
					{
						
						Coop_systemUI.tableData(search
								, (coopN.getValue() == null) ? 0 : new Integer(""+coopN.getValue())
								, (prod.getValue() == null) ? 0 : new Integer(""+prod.getValue())
								, ""+prodStatus.getValue());
					}
				}
			}
		});
        
        searchBtn.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if( ! interactive.getValue())
				{
					
					if( seachField.getValue().length() > 3 )
					{
						
						Coop_systemUI.tableData(seachField.getValue()
								, (coopN.getValue() == null) ? 0 : new Integer(""+coopN.getValue())
								, (prod.getValue() == null) ? 0 : new Integer(""+prod.getValue())
								, ""+prodStatus.getValue());
					} else
					{
						Notification.show("Search Error!"
								, "Ensure that the String is more than 3 character"
								, Notification.TYPE_ERROR_MESSAGE);
					}
				}
			}
		});
	}

	public String getSeachField() {
		return seachField.getValue();
	}

	public boolean getInteractive() {
		return interactive.getValue();
	}

	public String getCoopN() {
		return (String) coopN.getValue();
	}

	public String getProd() {
		return (String) prod.getValue();
	}

	public void setSeachField(String seachField) {
		this.seachField.setValue(seachField);
	}

	public void setInteractive(boolean interactive) {
		this.interactive.setValue(interactive);
	}

	public void setCoopN(String coopN) {
		this.coopN.setValue(coopN);
	}

	public void setProd(String prod) {
		this.prod.setValue(prod);
	}
}
