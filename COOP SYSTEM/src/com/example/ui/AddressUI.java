package com.example.ui;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class AddressUI extends VerticalLayout
{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HorizontalLayout horizontalL;
	
	private TextField product = new TextField();
	private ComboBox status = new ComboBox();
	private TextField coopOrg = new TextField();
	private TextField ageEnrolled = new TextField();
	private TextField benificiaries = new TextField();
	private TextField policyNum = new TextField();
	private TextField relationship = new TextField();
	private TextField coc = new TextField();
	private TextArea remarksForReceiving = new TextArea();
	private TextArea remarksForClaim = new TextArea();
	
	public AddressUI ()
	{
		
		setSpacing(true);
        setWidth("100%");
        setStyleName("product-info");
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");
        
        horizontalL.addComponent(product);
        product.setWidth("100%");
        product.setCaption("Product");
        product.setStyleName("product");
        
        horizontalL.addComponent(status);
        status.setWidth("100%");
        status.setCaption("Status");
        status.setStyleName("status");
        status.addItem(0);
        status.setItemCaption(0, "UNPAID");
        status.addItem(1);
        status.setItemCaption(1, "PAID");
        
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");
        
        horizontalL.addComponent(coopOrg);
        coopOrg.setWidth("100%");
        coopOrg.setCaption("Cooperative / Organization");
        coopOrg.setStyleName("coop-org");
        
        horizontalL.addComponent(ageEnrolled);
        ageEnrolled.setWidth("100%");
        ageEnrolled.setCaption("Age enrolled");
        ageEnrolled.setStyleName("age-enrolled");
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");
        
        horizontalL.addComponent(benificiaries);
        benificiaries.setWidth("100%");
        benificiaries.setCaption("Benificiaries");
        benificiaries.setStyleName("benificiaries");
        
        horizontalL.addComponent(policyNum);
        policyNum.setWidth("100%");
        policyNum.setCaption("Policy number");
        policyNum.setStyleName("policy-number");
        
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setSpacing(true);
        horizontalL.setWidth("100%");

        horizontalL.addComponent(relationship);
        relationship.setWidth("100%");
        relationship.setCaption("Relationship");
        relationship.setStyleName("relationship");
        
        horizontalL.addComponent(coc);
        coc.setWidth("100%");
        coc.setCaption("COC #");
        coc.setStyleName("coc-number");
        
        horizontalL = new HorizontalLayout();
        addComponent(horizontalL);
        horizontalL.setWidth("100%");
        horizontalL.setSpacing(true);
        
        horizontalL.addComponent(remarksForReceiving);
        remarksForReceiving.setWidth("100%");
        remarksForReceiving.setCaption("Remarks for Recieving");
        remarksForReceiving.setStyleName("remark-recieve");
        
        horizontalL.addComponent(remarksForClaim);
        remarksForClaim.setWidth("100%");
        remarksForClaim.setCaption("Remarks for Claim Dept.");
        remarksForClaim.setStyleName("remark-claim");
	}

	public String getProduct() {
		return product.getValue();
	}

	public String getStatus() {
		return (String) status.getValue();
	}

	public String getCoopOrg() {
		return coopOrg.getValue();
	}

	public String getAgeEnrolled() {
		return ageEnrolled.getValue();
	}

	public String getBenificiaries() {
		return benificiaries.getValue();
	}

	public String getPolicyNum() {
		return policyNum.getValue();
	}

	public String getRelationship() {
		return relationship.getValue();
	}

	public String getCoc() {
		return coc.getValue();
	}

	public String getRemarksForReceiving() {
		return remarksForReceiving.getValue();
	}

	public String getRemarksForClaim() {
		return remarksForClaim.getValue();
	}

	public void setProduct(String product) {
		this.product.setValue(product);
	}

	public void setStatus(int status) {
		this.status.setValue(status);
	}

	public void setCoopOrg(String coopOrg) {
		this.coopOrg.setValue(coopOrg);
	}

	public void setAgeEnrolled(String ageEnrolled) {
		this.ageEnrolled.setValue(ageEnrolled);
	}

	public void setBenificiaries(String benificiaries) {
		this.benificiaries.setValue(benificiaries);
	}

	public void setPolicyNum(String policyNum) {
		this.policyNum.setValue(policyNum);
	}

	public void setRelationship(String relationship) {
		this.relationship.setValue(relationship);
	}

	public void setCoc(String coc) {
		this.coc.setValue(coc);
	}

	public void setRemarksForReceiving(String remarksForReceiving) {
		this.remarksForReceiving.setValue(remarksForReceiving);
	}

	public void setRemarksForClaim(String remarksForClaim) {
		this.remarksForClaim.setValue(remarksForClaim);
	}
}
