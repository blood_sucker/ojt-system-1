package com.example.coop_system;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.servlet.annotation.WebServlet;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.model.ClaimsSubDetails;
import com.example.ui.AddressUI;
import com.example.ui.CoopUI;
import com.example.ui.SearchUI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("coopSystem")
public class Coop_systemUI extends UI {

	private HorizontalLayout horizontalLmain = new HorizontalLayout();
	public static CoopUI coopUI = new CoopUI();
	public static AddressUI addressUI = new AddressUI();
	private SearchUI searchUI = new SearchUI();
	private static Table data = new Table();
	private static VerticalLayout tableToolBar = new VerticalLayout();
	private static DBQuery select = new DBQuery(DBConnection.getConnection());
	
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = Coop_systemUI.class)
	public static class Servlet extends VaadinServlet {
	}
	
	@Override
	protected void init(VaadinRequest request) {
		final CssLayout layout = new CssLayout();
		layout.setSizeFull();
		layout.setStyleName("main-layout");
		Responsive.makeResponsive(layout);
		setContent(layout);
		
		Page.getCurrent().setTitle("CLAIMS INFORMATION");
		
		layout.addComponent(data);
		data.setWidth("93%");
		data.setVisible(false);
		data.setSelectable(true);
		data.setStyleName("data-table");
        data.addContainerProperty("claimant", String.class, null);
        data.setColumnHeader("claimant", "Claimant");
        data.addContainerProperty("status", String.class, null);
        data.setColumnHeader("status", "Status");
        data.addContainerProperty("product", String.class, null);
        data.setColumnHeader("product", "Product");
        
        layout.addComponent(tableToolBar);
        tableToolBar.setVisible(false);
        tableToolBar.setSpacing(true);
        tableToolBar.setSizeUndefined();
        tableToolBar.setStyleName("mini-toolbar");
        
        final Button hideBtn = new Button();
        tableToolBar.addComponent(hideBtn);
        hideBtn.setIcon(FontAwesome.MINUS);
        hideBtn.setDescription("Minimize table!");
        
        final Button closeBtn = new Button();
        tableToolBar.addComponent(closeBtn);
        closeBtn.setIcon(FontAwesome.THUMBS_UP);
        closeBtn.setDescription("Close table!");
        
		Button backLabel = new Button();
		layout.addComponent(backLabel);
		backLabel.setStyleName("back");
		backLabel.setIcon(FontAwesome.ARROW_LEFT);
		
		Button forwarLabel = new Button();
		layout.addComponent(forwarLabel);
		forwarLabel.setStyleName("forward");
		forwarLabel.setIcon(FontAwesome.ARROW_RIGHT);
		
		layout.addComponent(searchUI);
		
		
		final Button option = new Button();
		layout.addComponent(option);
		option.setIcon(FontAwesome.ARROW_DOWN);
		option.setStyleName("btn-option");
		
		
		layout.addComponent(horizontalLmain);
		horizontalLmain.setMargin(true);
		horizontalLmain.setSpacing(true);
		horizontalLmain.setWidth("100%");
		horizontalLmain.setStyleName("form-layout-h");
		
		horizontalLmain.addComponent(coopUI);
		horizontalLmain.addComponent(addressUI);
		
		backLabel.addClickListener(new Button.ClickListener() {
            
            @Override
            public void buttonClick(ClickEvent event) {
                    // TODO Auto-generated method stub
                    horizontalLmain.removeStyleName("view-coop-info");
                    horizontalLmain.addStyleName("view-prod-info");
                    
            }
		});
    
		forwarLabel.addClickListener(new Button.ClickListener() {
            
            @Override
            public void buttonClick(ClickEvent event) {
                    // TODO Auto-generated method stub
                    horizontalLmain.removeStyleName("view-prod-info");
                    horizontalLmain.addStyleName("view-coop-info");
            }
		});
		
		option.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if( searchUI.getStyleName().contains("more-search-bar") )
				{
					option.setIcon(FontAwesome.ARROW_DOWN);
					searchUI.removeStyleName("more-search-bar");
					option.removeStyleName("adjust-option");
					data.removeStyleName("adjust-option");
					tableToolBar.removeStyleName("adjust-option");
					horizontalLmain.removeStyleName("adjust-form");
				} else
				{
					option.setIcon(FontAwesome.ARROW_UP);
					searchUI.addStyleName("more-search-bar");
					option.addStyleName("adjust-option");
					data.addStyleName("adjust-option");
					tableToolBar.addStyleName("adjust-option");
					horizontalLmain.addStyleName("adjust-form");
				}
			}
		});
		
		
		closeBtn.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				data.setVisible(false);
				tableToolBar.setVisible(false);
			}
		});
		
		hideBtn.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				
				if( data.getStyleName().contains("zero-height") )
				{
					data.removeStyleName("zero-height");
					hideBtn.setIcon(FontAwesome.MINUS);
					hideBtn.setDescription("Minimize table!");
					closeBtn.setVisible(true);
				} else
				{
					
					data.addStyleName("zero-height");
					hideBtn.setIcon(FontAwesome.PLUS);
					hideBtn.setDescription("Maximize table!");
					closeBtn.setVisible(false);
				}
			}
		});
		
		
		data.addItemClickListener(new ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
				// TODO Auto-generated method stub
				ResultSet info = select.selectWithParam(
						ClaimsSubDetails.getClaimantInfo()
						, new String[] {""+event.getItemId()});
				
				if( event.isDoubleClick() && data.isVisible() )
				{
					
					data.setVisible(false);
					tableToolBar.setVisible(false);
				}
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				try
				{
					while(info.next())
					{
						
						coopUI.setClamantName(info.getString("name"));
						coopUI.setInsuredName(info.getString("principal"));
						coopUI.setDateOfBirth(formatter.parse(info.getString("dobirth")));
						coopUI.setDateFrom(formatter.parse(info.getString("dateFrom")));
						coopUI.setDateTo(formatter.parse(info.getString("dateTo")));
						coopUI.setNumberDay(info.getString("noDays"));
						coopUI.setCause(info.getString("cause"));
						addressUI.setProduct(info.getString("product"));
						addressUI.setStatus(new Integer(info.getString("prodStatus")));
						addressUI.setCoopOrg(info.getString("coopOrg"));
						addressUI.setAgeEnrolled(info.getString("Age_Enrolled"));
					}
					
					select.closePrepareStmt();
					select.closeResult();
					info.close();
				} catch (SQLException e)
				{
					e.printStackTrace();
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
	}
	
	
	public static void tableData ( String search, int coop, int product, String status )
	{
		
		String prodStatus = "";
		ResultSet dataTable = null;
		
		data.removeAllItems();
		dataTable  = select.searchClaimant(
							coop
							, product
							, search
							, status);
		try {
			
			if( !data.isVisible() ) { data.setVisible(true); tableToolBar.setVisible(true); }
			while(dataTable.next())
			{

				switch (new Integer(dataTable.getString("prod_status")))
				{

					case 0: prodStatus = "UNPAID"; break;
					case 1: prodStatus = "PAID"; break;
				}
				data.addItem(
						new Object[]{ dataTable.getString("name")
								, prodStatus
								, dataTable.getString("ProductLongName") }
							, new Integer(dataTable.getString("ClaimSubKeyNo"))
						);
			}
			select.closeResult();
			select.closePrepareStmt();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}