package com.example.model;

public class ClaimsSubDetails
{

	public static String getClaimantInfo ()
	{
		String sql = "SELECT CONCAT(firstnam, ' ', mi, '. ', lastnam) AS `name`"
				+ ", principal, dobirth"
				+ ", claimsindclient.`hosp_From` AS dateFrom"
				+ ", claimsindclient.`hosp_To` AS dateTo"
				+ ", claimsindclient.`days_hosp` AS noDays"
				+ ", claimsindclient.`cause`"
				+ ", product.`ProductLongName` AS product"
				+ ", groupclient.`Name` AS coopOrg"
				+ ", claimssubdetails.`policyno` AS policyNo"
				+ ", claimssubdetails.`Age_Enrolled`"
				+ ", product.`status` AS prodStatus "
				+ " FROM claimssubdetails "
				+ "INNER JOIN claimsindclient USING(ClaimKeyNo) "
				+ "LEFT OUTER JOIN product ON(product.`Product_no`=claimssubdetails.`productno`) "
				+ "INNER JOIN groupclient ON(groupclient.`CoopNum` = claimssubdetails.`coopnum`) WHERE claimssubdetails.ClaimSubKeyNo IN(?) LIMIT 1";
		return sql;
	}
}
