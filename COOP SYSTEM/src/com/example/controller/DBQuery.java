package com.example.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;


@SuppressWarnings("serial")
public class DBQuery implements Serializable
{

	public String PASS = "";
	private String sql = "";
	
	private Connection connection = null;
	private PreparedStatement preparedStmt = null;
	private Statement pStmt = null;
	private ResultSet result = null;
	
	public DBQuery ()
	{
		
		System.out.println("Connection must be set before using this Class");
	}
	
	public DBQuery (Connection iniCon)
	{
		
		this.connection = iniCon;
	}
	
	public void setConnection (Connection newCon)
	{
		
		this.connection = newCon;
	}

	
	public ResultSet select (String sql)
	{
		
		try
		{
			
			pStmt = this.connection.createStatement();
			result = pStmt.executeQuery(sql);
			return result;
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
	}
	
	
	public ResultSet selectWithParam (String sql, String[] param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			
			int i = 1;
			for(String p : param)
			{
				
				preparedStmt.setString(i, p);
				i++;
			}
			
			result = preparedStmt.executeQuery();
			return result;
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Insert or Update or Delete data on the specific query only
	 * @param sql query
	 * @param param value of the integer placeholder
	 * @return the number of rows changes
	 */
	public int insert (String sql, String[] param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			int i=1;
			for(String p : param )
			{
				
				preparedStmt.setString(i, p.trim());
				i++;
			}
			
			return preparedStmt.executeUpdate();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return 0;
		} finally 
		{
			try {
				preparedStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Insert or Update or Delete many data in one query
	 * @param sql the Data base query
	 * @param param value of the integer placeholder
	 * @return array of rows
	 */
	public int[] insertManyData (String sql, String[][] param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			this.setCommit(false);
			
			for(String[] p : param)
			{
				int i=1;
				for(String p1 : p)
				{
					
					preparedStmt.setString(i, p1.trim());
					i++;
				}
				preparedStmt.addBatch();
			}
			return preparedStmt.executeBatch();
			
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			this.rollBack();
			return null;
		} finally
		{
			
			try {
				preparedStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Insert or Update in different table
	 * @param sql array of query
	 * @return array of affected rows
	 */
	public int[] insertManyQuery (String[] sql)
	{
		
		
		try
		{
			
			this.setCommit(false);
			for(String query : sql)
			{
				
				pStmt.addBatch(query);
			}
			
			return pStmt.executeBatch();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			this.rollBack();
			return null;
		} finally
		{
			
			try {
				pStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public ResultSet searchClaimant ( int coop, int product, String searchName, String status )
	{
		
		String condition = "";
		ArrayList<String> parameters = new ArrayList<String>();
		String name = " AND `claimsindclient`.`firstNam` LIKE ? OR `claimsindclient`.`mi` LIKE ? OR `claimsindclient`.`lastnam` LIKE ?";
		sql = "SELECT"+
				"`ClaimSubKeyNo`"
				+ ", CONCAT(firstnam, ' ', UPPER(mi), '. ', lastnam ) AS name "
				+ ", `product`.`status` AS prod_status"
				+ ", `ProductLongName`"+
				"FROM claimssubdetails "+
					"INNER JOIN product ON(`product`.`Product_no`=`claimssubdetails`.`productno`) "+
					"INNER JOIN groupclient ON(`groupclient`.`CoopNum`=`claimssubdetails`.`coopnum`) "+
					"INNER JOIN `claimsindclient` ON(`claimsindclient`.`ClaimKeyNo` = `claimssubdetails`.`ClaimKeyNo`) ";
		if( searchName != "" )
		{
			
			if( coop > 0 && product > 0 )
			{
				
				condition = "WHERE `claimssubdetails`.`coopnum` IN(?) AND `claimssubdetails`.`productno` IN(?)";
				parameters.add(0,""+coop);
				parameters.add(1,""+product);
				if( status != "" )
				{
					
					condition += " AND `product`.`status` IN(?)" + name;
					parameters.add(2, status);
					parameters.add(3,searchName + "%");
					parameters.add(4,searchName + "%");
					parameters.add(5,searchName + "%");
				} else
				{
					
					condition += name;
					parameters.add(2,searchName + "%");
					parameters.add(3,searchName + "%");
					parameters.add(4,searchName + "%");
				}
			} else if ( coop > 0 && product < 1 )
			{
				
				condition = "WHERE `claimssubdetails`.`coopnum` IN(?)";
				parameters.add(0,""+coop);
				
				if( status != "" )
				{
					
					condition += " AND `product`.`status` IN(?)" + name;
					parameters.add(1, status);
					parameters.add(2,searchName + "%");
					parameters.add(3,searchName + "%");
					parameters.add(4,searchName + "%");
				} else
				{
					
					condition += name;
					parameters.add(1,searchName + "%");
					parameters.add(2,searchName + "%");
					parameters.add(3,searchName + "%");
				}
			} else if ( coop < 1 && product > 0 )
			{ 
				
				condition = "WHERE `claimssubdetails`.`productno` IN(?) AND `claimsindclient`.`firstNam` LIKE ? OR `claimsindclient`.`mi` LIKE ? OR `claimsindclient`.`lastnam` LIKE ?";
				parameters.add(0,""+product);
				parameters.add(1,searchName + "%");
				parameters.add(2,searchName + "%");
				parameters.add(3,searchName + "%");
				
				if( status != "" )
				{
					
					condition += " AND `product`.`status` IN(?)" + name;
					parameters.add(1, status);
					parameters.add(2,searchName + "%");
					parameters.add(3,searchName + "%");
					parameters.add(4,searchName + "%");
				} else
				{
					
					condition += name;
					parameters.add(1,searchName + "%");
					parameters.add(2,searchName + "%");
					parameters.add(3,searchName + "%");
				}
			}else
			{
				
				if( status != "" )
				{
					
					condition += " WHERE `product`.`status` IN(?)" + name;
					parameters.add(0, status);
					parameters.add(1,searchName + "%");
					parameters.add(2,searchName + "%");
					parameters.add(3,searchName + "%");
				} else
				{
					condition = "WHERE `claimsindclient`.`firstNam` LIKE ? OR `claimsindclient`.`mi` LIKE ? OR `claimsindclient`.`lastnam` LIKE ?";
					parameters.add(0,searchName + "%");
					parameters.add(1,searchName + "%");
					parameters.add(2,searchName + "%");
				}
			}
		}
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql + condition);
			int i = 1;
			for(String p : parameters)
			{
				
				preparedStmt.setString(i, p);
				i++;
			}
			
			result = preparedStmt.executeQuery();
			return result;
			
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
		
	}
	
	public Boolean isRunningResult ()
	{
		try
		{
			return result.isClosed();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public void closeResult ()
	{
		
		try
		{
			
			result.close();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
		}
	}
	
	public Boolean isRunningStatement ()
	{
		
		try
		{
			
			return pStmt.isClosed();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
	}
	
	
	public void closeStatement ()
	{
		
		try
		{
			
			pStmt.close();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
		}
	}
	
	
	public boolean isRunningPrepareStatement ()
	{
		
		try
		{
			
			return preparedStmt.isClosed();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Close the preparedStatement
	 */
	public void closePrepareStmt ()
	{
		
		try
		{
			
			preparedStmt.close();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
		}
	}
	
	/**
	 * Check if the query is auto commit
	 * @return boolean status of autocommit
	 */
	public boolean isAutoCommit ()
	{
		
		try {
			
			return this.connection.getAutoCommit(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Set the commit
	 * @param commit to On and Off the commit operation
	 */
	public void setCommit (boolean commit)
	{
		
		try {
			this.connection.setAutoCommit(commit);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * this use to commit the query
	 */
	public void commitBatch ()
	{
		
		try {
			this.connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * rollback queries
	 */
	public void rollBack ()
	{
		
		try {
			this.connection.rollback();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
