package com.example.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.example.coop_information.Coop_informationUI;
import com.example.model.GroupClient;
import com.example.ui.ActionsUI;
import com.vaadin.ui.Notification;


public class Functions
{

	private static DBQuery QUERY = new DBQuery(DBConnection.getConnection());
	private static ResultSet RESULT = null;
	private static ArrayList<String> VALUES = new ArrayList<String>();
	private static ArrayList<String> COLUMNS = new ArrayList<String>();
	
	public static ResultSet searchInfo (String business, int group, int branch, String search )
	{
		
		String sql = "SELECT * FROM groupclient";
		String name = "Name LIKE ? OR Branch LIKE ?";
		
		ArrayList<String> parameters = new ArrayList<String>();
		ArrayList<String> conditions = new ArrayList<String>();
		
		if( ! business.equals("-1") )
		{
			conditions.add("BusinessType IN(?)");
			parameters.add(business);
		}
		if( group != -1 )
		{
			conditions.add("Group_id IN(?)");
			parameters.add(""+group);
		}
		// Reserve for branch
//		if( branch != -1 )
//		{
//			conditions.addAll("")
//		}
		
		if( conditions.size() > 0 )
		{
			conditions.add(name);
			parameters.add(search + "%");
			parameters.add(search + "%");
			String join = " WHERE " + CollectionUtils.join(conditions, " AND ");
			sql += join;
		} else
		{
			parameters.add(search + "%");
			parameters.add(search + "%");
			sql += " WHERE " + name;
		}
		
		RESULT = QUERY.statement(sql, parameters);
		
		return RESULT;
	}
	
	
	public static boolean isInteger ( String s )
	{
		try
		{
			Integer.parseInt(s);
		} catch (NumberFormatException e)
		{
			return false;
		} catch (NullPointerException e)
		{
			return false;
		}
		
		return true;
	}
	
	
	public static ArrayList<String> location ( String s )
	{
		ArrayList<String> data = new ArrayList<String>();
		RESULT = QUERY.statement(GroupClient.getOne("Province, Region, Area"), new String[] {s});
		try
		{
			while(RESULT.next())
			{
				data.add(RESULT.getString("Province").trim());
				data.add(RESULT.getString("Region"));
				data.add(RESULT.getString("Area"));
			}
			RESULT.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally {
			QUERY.closePrepareStmt();
			QUERY.closeResult();
		}
		
		return data;
	}
	
	
	public static void updateInfo (String coopId)
	{
		
		String coop = Coop_informationUI.info.getCoopName();
		if( coopId != null )
		{
			setData();
			VALUES.add(coopId);
			QUERY.pStmtRows(GroupClient.update(CollectionUtils.join(COLUMNS, ", ", " = ?")), VALUES);
			QUERY.closePrepareStmt();
			ActionsUI.clickCancel();
			VALUES.clear();
			COLUMNS.clear();
			Notification.show( coop + " was SUCCESSFULY UPDATED!", Notification.TYPE_TRAY_NOTIFICATION);
		} else
		{
			RESULT = QUERY.statement(GroupClient.getAvailableId());
			try
			{
				while(RESULT.next())
				{
					VALUES.add(RESULT.getString("available_id"));
					COLUMNS.add("CoopNum");
					System.out.println(RESULT.getString("available_id"));
				}
				RESULT.close();
			} catch (SQLException e)
			{
				e.printStackTrace();
			} finally {
				QUERY.closeStatement();
				QUERY.closeResult();
				setData();
				if( VALUES.size() > 1 )
				{
					QUERY.pStmtRows(GroupClient.insertInfo(CollectionUtils.join(COLUMNS, ", "), CollectionUtils.createLoop(COLUMNS.size(), "?", ", ")), VALUES);
					QUERY.closePrepareStmt();
					ActionsUI.clickCancel();
					VALUES.clear();
					COLUMNS.clear();
					Notification.show( coop + " was SUCCESSFULY SAVED!", Notification.TYPE_TRAY_NOTIFICATION);
				} else
				{
					Notification.show("Fields are empty!", Notification.TYPE_ERROR_MESSAGE);
					VALUES.clear();
					COLUMNS.clear();
				}
			}
		}
	}
	
	
	private static void setData ()
	{
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if( !Coop_informationUI.info.getCoopName().isEmpty() )
		{
			VALUES.add(Coop_informationUI.info.getCoopName());
			COLUMNS.add("`Name`");
		}
	
		if( Coop_informationUI.info.getBranch() != null )
		{
			VALUES.add(Coop_informationUI.info.getBranch());
			COLUMNS.add("Branch");
		}
		if( !Coop_informationUI.info.getAcronm().isEmpty() )
		{
			VALUES.add(Coop_informationUI.info.getAcronm());
			COLUMNS.add("Acronym");
		}
		if( !Coop_informationUI.address.getBarangay().isEmpty() )
		{
			VALUES.add(Coop_informationUI.address.getBarangay());
			COLUMNS.add("Barangay");
		}
		if( Coop_informationUI.address.getCityCaption() != null )
		{
			VALUES.add(Coop_informationUI.address.getCityCaption());
			COLUMNS.add("MunOrCity");
		}
		if( Coop_informationUI.address.getProvince() != null )
		{
			VALUES.add(Coop_informationUI.address.getProvince());
			COLUMNS.add("Province");
		}
		if( !Coop_informationUI.address.getRegion().isEmpty() )
		{
			VALUES.add(Coop_informationUI.address.getRegion());
			COLUMNS.add("Region");
		}
		if( !Coop_informationUI.address.getArea().isEmpty() )
		{
			VALUES.add(Coop_informationUI.address.getArea());
			COLUMNS.add("Area");
		}
		if( Coop_informationUI.address.getZipcode() != null )
		{
			if( !Coop_informationUI.address.getZipcode().isEmpty() )
			{
				VALUES.add(Coop_informationUI.address.getZipcode());
				COLUMNS.add("ZipCode");
			}
		}
		if( Coop_informationUI.info.getBusiness() != null )
		{
			if( !Coop_informationUI.info.getBusiness().isEmpty() )
			{
				VALUES.add(Coop_informationUI.info.getBusiness());
				COLUMNS.add("BusinessType");	
			}
		}
		if( !Coop_informationUI.info.getTin().isEmpty() )
		{
			VALUES.add(Coop_informationUI.info.getTin());
			COLUMNS.add("TIN");
		}
		if( Coop_informationUI.info.getDateStart() != null)
		{
			VALUES.add(format.format(Coop_informationUI.info.getDateStart()));
			COLUMNS.add("datestarted");
		}
		if( !Coop_informationUI.info.getCdaNum().isEmpty() )
		{
			VALUES.add(Coop_informationUI.info.getCdaNum());
			COLUMNS.add("cda_registration_no");
		}
		if( Coop_informationUI.info.getDateReg() != null )
		{
			VALUES.add(format.format(Coop_informationUI.info.getDateReg()));
			COLUMNS.add("Date_Registered");
		}
		if( !Coop_informationUI.address.getRemarks().isEmpty() ) 
		{
			VALUES.add(Coop_informationUI.address.getRemarks());
			COLUMNS.add("remarks");
		}
		if( !Coop_informationUI.info.getCoopNum().isEmpty() ) 
		{
			VALUES.add(Coop_informationUI.info.getCoopNum());
			COLUMNS.add("coop_id");
		}
		if( !Coop_informationUI.address.getAddress().isEmpty() ) 
		{
			VALUES.add(Coop_informationUI.address.getAddress());
			COLUMNS.add("Address");
		}
		if( Coop_informationUI.info.getGroup() != null ) 
		{
			if( !Coop_informationUI.info.getGroup().isEmpty() )
			{
				VALUES.add(Coop_informationUI.info.getGroup());
				COLUMNS.add("Group_id");
			}
		}
		if( !Coop_informationUI.info.getCoopCode().isEmpty() ) 
		{
			VALUES.add(Coop_informationUI.info.getCoopCode());
			COLUMNS.add("CoopCode");
		}

	}

}
