package com.example.controller;

import java.util.AbstractCollection;
import java.util.Iterator;

public class CollectionUtils
{

	public static String join (AbstractCollection<String> s)
	{
		
		return CollectionUtils.join(s, "");
	}
	
	public static String join (AbstractCollection<String> s, String delimeter)
	{
		
		if( s == null || s.isEmpty() ) return "";
		Iterator<String> etir = s.iterator();
		StringBuilder builder = new StringBuilder(etir.next());
		while( etir.hasNext() )
		{
			builder.append(delimeter).append(etir.next());
		}
		
		return builder.toString();
	}
	
	
	public static String join (AbstractCollection<String> s, String delimeter, String after)
	{
		if( s == null || s.isEmpty() ) return "";
		Iterator<String> etir = s.iterator();
		StringBuilder builder = new StringBuilder(etir.next() + after);
		while( etir.hasNext() )
		{
			builder.append(delimeter).append(etir.next()).append(after);
		}
		
		return builder.toString();
	}
	
	
	public static String createLoop (int length, String content, String delimeter)
	{
		if( length == 0 ) return "";
		StringBuilder builder = new StringBuilder(content);

		for(int i=1; i<length; i++)
		{
			builder.append(delimeter).append(content);
		}

		return builder.toString();
	}
}
