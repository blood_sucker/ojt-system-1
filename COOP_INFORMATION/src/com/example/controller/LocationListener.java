package com.example.controller;

import java.util.ArrayList;

import com.example.coop_information.Coop_informationUI;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

@SuppressWarnings("serial")
public class LocationListener implements ValueChangeListener
{

	@Override
	public void valueChange(ValueChangeEvent event)
	{
		String string = (String) event.getProperty().getValue();
		if( string != null )
		{

			Coop_informationUI.address.setCity(string);
			Coop_informationUI.address.setZipcode(string);
			
			ArrayList<String> data = Functions.location(string);
			
			Coop_informationUI.address.setProvince(data.get(0));
			Coop_informationUI.address.setRegion(data.get(1));
			Coop_informationUI.address.setArea(data.get(2));
		}
	}
}
