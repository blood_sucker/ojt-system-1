package com.example.controller;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.coop_information.Coop_informationUI;
import com.example.model.GroupClient;
import com.example.ui.ActionsUI;
import com.example.ui.SearchWindow;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

@SuppressWarnings("serial")
public class TableRowListener implements ItemClickListener
{

	@Override
	public void itemClick (ItemClickEvent event)
	{
		
		String coopNum = null;
		ResultSet info = SearchWindow.query.statement(GroupClient.selectCoopInfoId(), new String[] { ""+event.getItemId() });
		try
		{
			while(info.next())
			{
				Coop_informationUI.info.setCoopName(info.getString("Name"));
				Coop_informationUI.info.setAcronm(info.getString("Acronym"));
				Coop_informationUI.info.setCoopNum(info.getString("coop_id"));
				Coop_informationUI.info.setBranch(info.getString("Branch"));
				Coop_informationUI.info.setBusiness(info.getString("BusinessType"));
				Coop_informationUI.info.setGroup(info.getString("Group_id"));
				Coop_informationUI.info.setTin(info.getString("TIN"));
				Coop_informationUI.info.setCoopCode(info.getString("CoopCode"));
				Coop_informationUI.info.setCdaNum(info.getString("cda_registration_no"));
				Coop_informationUI.info.setDateReg(info.getString("Date_Registered"));
				Coop_informationUI.info.setDateStart(info.getString("datestarted"));
				Coop_informationUI.address.setAddress(info.getString("Address"));
				Coop_informationUI.address.setBarangay(info.getString("Barangay"));
				Coop_informationUI.address.setRegion(info.getString("Region"));
				Coop_informationUI.address.setArea(info.getString("Area"));
				Coop_informationUI.address.setRemarks(info.getString("remarks"));
				Coop_informationUI.address.setZipcode(info.getString("ZipCode"));
				coopNum = info.getString("CoopNum").trim();
			}
			info.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally {
			SearchWindow.query.closePrepareStmt();
			SearchWindow.query.closeResult();
			ActionsUI.setSaveId(coopNum);
			ActionsUI.setSaveCaption("UPDATE");
		}
	}
}
