package com.example.controller;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.ui.SearchWindow;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;

@SuppressWarnings("serial")
public class SearchTextChange implements TextChangeListener
{

	
	@Override
	public void textChange (TextChangeEvent event)
	{
		
		if( event.getText().length() > 3 )
		{
			
			SearchWindow.table.removeAllItems();
			ResultSet searchCoop = Functions.searchInfo(
					""+SearchWindow.businessType.getValue()
					, new Integer(SearchWindow.groupOf.getValue()+"")
					, new Integer(SearchWindow.branchType.getValue()+"")
					, event.getText());
			
			try {
				while(searchCoop.next())
				{
					
					SearchWindow.table.addItem(new Object[]{ searchCoop.getString("coop_id")
							, searchCoop.getString("Name")
							, searchCoop.getString("Branch")
							, searchCoop.getString("BusinessType")}, new Integer(searchCoop.getInt("CoopNum")));
					
				}
				searchCoop.close();					
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 	
			} finally {
				SearchWindow.query.closeStatement();
				SearchWindow.query.closeResult();
			}
		}
	}
}
