package com.example.controller;

import org.vaadin.dialogs.ConfirmDialog;

import com.example.model.GroupClient;
import com.example.ui.SearchWindow;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class DeleteListener implements ClickListener
{

	@Override
	public void buttonClick(ClickEvent event)
	{
		
//		System.out.println(event.getButton().getId());
		final String id = event.getButton().getId().trim();
		ConfirmDialog.show(UI.getCurrent()
				, "Confirmation!"
				, "Are you sure you want to delete this Client ?"
				, "YES!"
				, "NO"
				, new ConfirmDialog.Listener() {
					
					@Override
					public void onClose(ConfirmDialog arg0) {
						// TODO Auto-generated method stub
						if ( arg0.isConfirmed() )
						{
							int confirm = SearchWindow.query.pStmtRows(GroupClient.deleteGroup(), new String[] {id});
							
							if( confirm == 0 )
							{
								Notification.show("Failure in deleting Client!", Notification.TYPE_ERROR_MESSAGE);
							} else
							{
								SearchWindow.table.removeItem(new Integer(id));
							}
						}
					}
				});
	}
}
