	/**
	 *	Cancel Button
	 * 
	 *	if button is click all fields will be clear 
	 *
	 */
package com.example.controller;

import com.example.coop_information.Coop_informationUI;
import com.example.ui.ActionsUI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")


public class CancelListener implements ClickListener
{

	@Override
	public void buttonClick(ClickEvent event)
	{
		ActionsUI.setSaveId("save");
		ActionsUI.setSaveCaption("SAVE");
		Coop_informationUI.info.clear();
		Coop_informationUI.address.clear();
	}
}
