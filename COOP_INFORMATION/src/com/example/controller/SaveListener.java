package com.example.controller;

import com.example.ui.ActionsUI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class SaveListener implements ClickListener 
{

	@Override
	public void buttonClick (ClickEvent event)
	{
		
		if( Functions.isInteger(ActionsUI.getSaveId()) )
		{
			Functions.updateInfo(ActionsUI.getSaveId());
		} else
		{
			if( ActionsUI.getSaveId().equals("save") )
			{
				Functions.updateInfo(null);
			}
		}
	}
}
