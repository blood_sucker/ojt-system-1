	/**
	 * 
	 *	Database Query Class 
	 *
	 */

package com.example.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;



@SuppressWarnings("serial")
public class DBQuery implements Serializable
{
	
	private Connection connection = null;
	private PreparedStatement preparedStmt = null;
	private Statement pStmt = null;
	private ResultSet result = null;
	
	public DBQuery ()
	{
		
		System.out.println("Connection must be set before using this Class");
	}
	
	public DBQuery (Connection iniCon)
	{
		
		this.connection = iniCon;
	}
	
	public void setConnection (Connection newCon)
	{
		
		this.connection = newCon;
	}
	
	
	public ResultSet statement (String sql)
	{
		
		try
		{
			
			pStmt = this.connection.createStatement();
			result = pStmt.executeQuery(sql);
			return result;
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param sql query
	 * @param param 
	 * @return Database result
	 */
	public ResultSet statement (String sql, String[] param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			
			int i = 1;
			for(String p : param)
			{
				
				preparedStmt.setString(i, p);
				i++;
			}
			
			result = preparedStmt.executeQuery();
			return result;
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Method that the variable param is Array List Object
	 * @param sql string
	 * @param param arraylist
	 * @return query result
	 */
	public ResultSet statement (String sql, ArrayList<String> param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			int i=1;
			for(String p : param )
			{
				
				preparedStmt.setString(i, p.trim());
				i++;
			}
			
			return preparedStmt.executeQuery();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		} finally 
		{
			try {
				preparedStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Insert or Update or Delete data on the specific query only
	 * @param sql query
	 * @param param value of the integer placeholder
	 * @return the number of rows changes
	 */
	public int pStmtRows (String sql, String[] param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			int i=1;
			for(String p : param )
			{
				
				preparedStmt.setString(i, p.trim());
				i++;
			}
			
			return preparedStmt.executeUpdate();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return 0;
		} finally 
		{
			try {
				preparedStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public int pStmtRows (String insertSql, ArrayList<String> param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(insertSql);
			int i=1;
			for(String p : param )
			{
				preparedStmt.setString(i, p.trim());
				i++;
			}
			return preparedStmt.executeUpdate();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return 0;
		} finally 
		{                              
			try {
				preparedStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	/**
	 * Insert or Update or Delete many data in one query
	 * @param sql the Data base query
	 * @param param value of the integer placeholder
	 * @return array of rows
	 */
	public int[] pStmtMultiQ (String sql, String[][] param)
	{
		
		try
		{
			
			preparedStmt = this.connection.prepareStatement(sql);
			this.setCommit(false);
			
			for(String[] p : param)
			{
				int i=1;
				for(String p1 : p)
				{
					
					preparedStmt.setString(i, p1.trim());
					i++;
				}
				preparedStmt.addBatch();
			}
			return preparedStmt.executeBatch();
			
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			this.rollBack();
			return null;
		} finally
		{
			
			try {
				preparedStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Insert or Update in different table
	 * @param sql array of query
	 * @return array of affected rows
	 */
	public int[] pStmtMultiQ (String[] sql)
	{
		
		
		try
		{
			
			this.setCommit(false);
			for(String query : sql)
			{
				
				pStmt.addBatch(query);
			}
			
			return pStmt.executeBatch();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			this.rollBack();
			return null;
		} finally
		{
			
			try {
				pStmt.clearBatch();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Checking for the result set.
	 * @return
	 */
	public Boolean isRunningResult ()
	{
		try
		{
			return result.isClosed();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public void closeResult ()
	{
		
		try
		{
			
			result.close();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
		}
	}
	
	public Boolean isRunningStatement ()
	{
		
		try
		{
			
			return pStmt.isClosed();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return null;
		}
	}
	
	
	public void closeStatement ()
	{
		
		try
		{
			
			pStmt.close();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
		}
	}
	
	
	public boolean isRunningPrepareStatement ()
	{
		
		try
		{
			
			return preparedStmt.isClosed();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Close the preparedStatement
	 */
	public void closePrepareStmt ()
	{
		
		try
		{
			
			preparedStmt.close();
		} catch (SQLException e)
		{
			
			e.printStackTrace();
		}
	}
	
	/**
	 * Check if the query is auto commit
	 * @return boolean status of autocommit
	 */
	public boolean isAutoCommit ()
	{
		
		try {
			
			return this.connection.getAutoCommit(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Set the commit
	 * @param commit to On and Off the commit operation
	 */
	public void setCommit (boolean commit)
	{
		
		try {
			this.connection.setAutoCommit(commit);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * this use to commit the query
	 */
	public void commitBatch ()
	{
		
		try {
			this.connection.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * rollback queries
	 */
	public void rollBack ()
	{
		
		try {
			this.connection.rollback();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
