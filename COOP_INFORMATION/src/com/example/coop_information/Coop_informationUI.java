package com.example.coop_information;

import javax.servlet.annotation.WebServlet;

import com.example.ui.ActionsUI;
import com.example.ui.AddressUI;
import com.example.ui.InfoUI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("coop_information")
public class Coop_informationUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = Coop_informationUI.class)
	public static class Servlet extends VaadinServlet {
	}
	
	public static InfoUI info = new InfoUI();
	public static AddressUI address = new AddressUI();
	public static ActionsUI actions = new ActionsUI();

	@Override
	protected void init(VaadinRequest request) {
		final CssLayout mainLayout = new CssLayout();
		
		Page.getCurrent().setTitle("Coop Information Sysytem");
		
		mainLayout.addStyleName("main-layout");
		mainLayout.setSizeFull();
		Responsive.makeResponsive(mainLayout);
		
		setContent(mainLayout);
		
		HorizontalLayout layout = new HorizontalLayout();
		layout.setSizeUndefined();
		mainLayout.addComponent(layout);
		
		layout.addComponent(actions);
		HorizontalLayout formLayout = new HorizontalLayout();
		formLayout.setSizeUndefined();;
		formLayout.setSpacing(true);
		formLayout.addStyleName("form-layout");
		layout.addComponent(formLayout);
		
		formLayout.addComponent(info);
		formLayout.addComponent(address);
	}

}