package com.example.model;


public class GroupClient
{

	private static String table = " groupclient ";
	
	public static String getInfo ()
	{
		
		String q = "SELECT CoopNum, Branch, BusinessType, Name FROM" + table;
		
		return q;
	}
	
	public static String selectCoopInfoId()
	{
		String q = "SELECT * FROM" + table +"WHERE CoopNum IN(?) LIMIT 1";
		
		return q;
	}
	
	public static String getColumnByGroup(String column, String group)
	{
		String q = "SELECT " + column + " FROM" + table + "GROUP BY " + group;
		
		return q;
	}
	
	
	public static String getOne (String columns)
	{
		String q = "SELECT " + columns + " FROM" + table + "WHERE ZipCode IN(?) LIMIT 1";
		
		return q;
	}
	
	/**
	 * update info
	 * 
	 * Name
	 * Branch
	 * Acronym
	 * Barangay
	 * MunOrCity
	 * Province
	 * Region
	 * Area
	 * ZipCode
	 * BusinessType
	 * TIN
	 * datestarted
	 * cda_registration_no
	 * Date_Registered
	 * remarks
	 * CoopCode
	 * Address
	 * Group_id
	 * coop_id
	 * 
	 * WHERE CoopNum
	 * @return
	 */
	public static String update (String columns)
	{
		
		String q = "UPDATE" + table + " SET "
				+ columns
				+ " WHERE CoopNum IN(?) LIMIT 1";

		return q;
	}
	
	public static String insertInfo (String columns, String values)
	{
		String q = "INSERT INTO" + table + "("
				+ columns
				+ ") VALUES ("
				+ values
				+ ") ";
		
		return q;
	}
	
	
	public static String getAvailableId ()
	{
		String q = "SELECT CoopNum + 1 available_id\n"
					+"FROM groupclient t\n"
						+"WHERE NOT EXISTS\n"
						+"(\n"
							+"SELECT CoopNum\n"
								+"FROM groupclient\n"
								+"WHERE CoopNum = t.CoopNum + 1\n"
						+")\n"
					+ "ORDER BY CoopNum\n"
					+ "LIMIT 1";
		return q;
	}
	
	
	public static String deleteGroup ()
	{
		String q = "DELETE FROM " + table + " WHERE CoopNum IN(?) LIMIT 1";
		return q;
	}
}
