package com.example.model;

import com.example.controller.DBConnection;

import java.sql.*;

public class CoopInfo {
	
	@SuppressWarnings("unused")
	private static Connection con = DBConnection.getConnection();
	
	
	/*
	 * SELECT ALL COLUMNS FROM TABLE groupclient
	 * @return
	 */
	public static String selectCoopInfo()
	{
		String q = "SELECT * FROM groupclient";
		
		return q;
	}
	
	public static String selectCoopInfoId()
	{
		String q = "SELECT * FROM groupclient WHERE CoopNum IN(?) LIMIT 1";
		
		return q;
	}
	
	public static String selectGroupOf()
	{
		String q = "SELECT * FROM groupclient INNER JOIN groupcategory " ;
		return q;
	}
	
	/*
	 * SEARCHING FOR ADDRESS
	 */
	public static String searchAddress(String search)
	{
		String q = "SELECT * FROM groupclient WHERE ZipCode LIKE '" + search + "%' OR MunOrCity LIKE '"+ search + "%' OR Province LIKE '"+ search + "%'";
		return q;
	}
	
	public static String searchCoopInfo(String searchCoop)
	{
		String q = "SELECT * FROM groupclient WHERE Name LIKE '"+ searchCoop +"%' OR Branch LIKE '"+ searchCoop +"%' ";
		return q;
	}

	/**
	 * MainCode, Name, coop_id, Branch, Acronym, BusinessType, Group_id,
	 * Street, Barangay, MunOrCity, Province, Region, Area, ZipCode
	 * @return
	 */
	
	public static String insertCoopInfo()
	{
		String q =  " INSERT INTO groupclient " +
					" ( Name, coop_id, Acronym, Branch, BusinessType, Group_id " +
					" ,Street, Barangay, ZipCode, MunOrCity, Province, Region, Area )" +
					" VALUES (  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
		
		return q;
	}
	
	public static String updateCoopInfo()
	{
		String 	q = " UPDATE groupclient " +
					" SET "
					+ "coop_id = ?, Acronym = ?, Branch = ?, BusinessType = ?, Group_id = ?" +
					" ,Street = ?, Barangay = ?, ZipCode = ?, MunOrCity = ?, Province = ?, Region = ?, Area = ?" +
					" WHERE CoopNum IN(?) LIMIT 1 ";
		
		return q;
	}
	
	public static String deleteCoopInfo()
	{
		String q = "DELETE FROM groupclient WHERE CoopNum IN(?) LIMIT 1";
		
		return q;
	}
	
}
