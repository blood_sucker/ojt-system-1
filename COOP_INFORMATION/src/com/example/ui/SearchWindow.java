package com.example.ui;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.controller.DeleteListener;
import com.example.controller.SearchTextChange;
import com.example.controller.TableRowListener;
import com.example.model.GroupCategory;
import com.example.model.GroupClient;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.AbstractTextField.TextChangeEventMode;

@SuppressWarnings("serial")
public class SearchWindow extends Window
{
	 
	public static DBQuery query = new DBQuery(DBConnection.getConnection());
	private HorizontalLayout horizontalL;
	public static final Table table = new Table();
	public static ComboBox groupOf = new ComboBox();
	public static ComboBox branchType = new ComboBox();
	public static ComboBox businessType = new ComboBox();
	
	public SearchWindow(){
		center();
		setIcon(FontAwesome.SEARCH_PLUS);
		setCaption(" Search Window");
		setHeight("90%");
		setWidth("80%");
		setImmediate(true);
		setDraggable(true);
		setResizable(false);
		
		VerticalLayout form = new VerticalLayout();
		form.setSpacing(true);
		form.setMargin(true);
		setContent(form);
		
		horizontalL = new HorizontalLayout();
		form.addComponent(horizontalL);
		horizontalL.setStyleName("search-bar");
		horizontalL.setWidth("100%");
		
		TextField search = new TextField();
		horizontalL.addComponent(search);
		search.setInputPrompt("Search Here ......");
		search.setStyleName("search-field");
		search.setWidth("100%");
		search.setTextChangeEventMode(TextChangeEventMode.EAGER);
		search.addTextChangeListener(new SearchTextChange());
		search.focus();
		
		Button submitSearch = new Button();
		horizontalL.addComponent(submitSearch);
		submitSearch.setCaption("Submit");
		submitSearch.setIcon(FontAwesome.SEARCH);
		submitSearch.setClickShortcut(KeyCode.ENTER);
		submitSearch.setStyleName("search-button");
		
		horizontalL = new HorizontalLayout();
		form.addComponent(horizontalL);
		horizontalL.setSpacing(true);
		horizontalL.setWidth("100%");
		
		horizontalL.addComponent(businessType);
		businessType.setCaption("Business type");
		businessType.setWidth("100%");
		businessType.setStyleName("business-type");
		businessType.setNullSelectionAllowed(false);
		
		horizontalL.addComponent(groupOf);
		groupOf.setCaption("Group");
		groupOf.setWidth("100%");
		groupOf.setStyleName("group-of");
		groupOf.setNullSelectionAllowed(false);
		
		horizontalL.addComponent(branchType);
		branchType.setCaption("Branch type");
		branchType.setWidth("100%");
		branchType.setStyleName("branch-type");
		branchType.setNullSelectionAllowed(false);
		branchType.addItem(-1);
		branchType.addItem(2);
		branchType.addItem(3);
		branchType.setItemCaption(-1, "BOOTH");
		branchType.setItemCaption(2, "MAIN");
		branchType.setItemCaption(3, "BRANCH");
		branchType.setValue(-1);
		
		form.addComponent(table);
		table.setPageLength(8);
		table.setWidth("100%");
		table.setSelectable(true);
		table.addContainerProperty("Coop #", String.class, null);
		table.addContainerProperty("Coop Name", String.class, null);
		table.addContainerProperty("Branch", String.class, null);
		table.addContainerProperty("Business Type", String.class, null);
		table.addItemClickListener(new TableRowListener());
		if( table.getColumnGenerator("Actions") == null )
		{
			
			table.addGeneratedColumn("Actions", new Table.ColumnGenerator() {
				
				@Override
				public Object generateCell(Table source, Object itemId, Object columnId) {
					// TODO Auto-generated method stub
					
					Button removeBtn = new Button();
					removeBtn.setCaption("Delete");
					removeBtn.setIcon(FontAwesome.ERASER);
					removeBtn.setId(itemId+"");

					removeBtn.addClickListener(new DeleteListener());
					return removeBtn;
				}
			});
		}
		
		
		ResultSet business = query.statement(GroupClient.getColumnByGroup("BusinessType", "BusinessType"));
		try
		{
			
			businessType.addItem(-1);
			businessType.setItemCaption(-1, "All");
			businessType.setValue(-1);
			while(business.next())
			{
				businessType.addItem(business.getString("BusinessType"));
			}
			business.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
		
		ResultSet clientGroup = query.statement(GroupCategory.getGroup());
		try
		{
			groupOf.addItem(-1);
			groupOf.setItemCaption(-1, "All");
			groupOf.setValue(-1);
			while(clientGroup.next())
			{
				
				groupOf.addItem(clientGroup.getString("Group_id"));
				groupOf.setItemCaption(clientGroup.getString("Group_id"), clientGroup.getString("Description"));
			}
			clientGroup.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
	}

}
