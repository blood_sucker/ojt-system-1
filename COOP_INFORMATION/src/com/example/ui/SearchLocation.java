package com.example.ui;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.model.CoopInfo;
import com.example.coop_information.Coop_informationUI;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class SearchLocation extends Window{
	
	DBQuery query = new DBQuery(DBConnection.getConnection());
	
	public SearchLocation() {

		center();
		setIcon(FontAwesome.SEARCH_PLUS);
		setCaption(" Search Window");
		setHeight("70%");
		setWidth("40%");
		setImmediate(true);
		setDraggable(true);
		setResizable(false);
		
		FormLayout form = new FormLayout();
		setContent(form);
		
		final Table table = new Table();
		table.setWidth("100%");
		table.setPageLength(8);
		table.setSelectable(true);
		
		table.addContainerProperty("ZipCode", String.class, null);
		table.addContainerProperty("City", String.class, null);
		table.addContainerProperty("Region", String.class, null);
		table.addContainerProperty("Area", String.class, null);
		
		ResultSet select = query.statement(CoopInfo.selectCoopInfo());
		try {
			while(select.next())
			{
				table.addItem(new Object[]{ select.getString("ZipCode"), select.getString("MunOrCity"), select.getString("Region"), select.getString("Area")}, new Integer(select.getInt("CoopNum")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TextField search = new TextField();
		search.setInputPrompt("Search Here ... ");
		search.addTextChangeListener(new TextChangeListener() {
			
			@Override
			public void textChange(TextChangeEvent event) {
				// TODO Auto-generated method stub
				table.removeAllItems();
				
				ResultSet search = query.statement(CoopInfo.searchAddress(event.getText()));
				try {
					while(search.next())
					{
						
						table.addItem(new Object[]{ search.getString("ZipCode"), search.getString("MunOrCity"), search.getString("Region"), search.getString("Area")}, new Integer(search.getInt("CoopNum")));
						
					}
					search.close();					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		table.addItemClickListener(new ItemClickEvent.ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
				// TODO Auto-generated method stub
				Coop_informationUI.address.setZipcode((String) event.getItem().getItemProperty("ZipCode").getValue());
				Coop_informationUI.address.setCity((String) event.getItem().getItemProperty("City").getValue());
				Coop_informationUI.address.setProvince((String) event.getItem().getItemProperty("Province").getValue());
				Coop_informationUI.address.setRegion((String) event.getItem().getItemProperty("Region").getValue());
				Coop_informationUI.address.setArea((String) event.getItem().getItemProperty("Area").getValue());
			}
		});
		
		form.addComponent(search);
		form.addComponent(table);
		
	}

}
