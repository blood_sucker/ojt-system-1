package com.example.ui;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.model.CoopInfo;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class MyWindow extends Window {
	
	DBQuery query = new DBQuery(DBConnection.getConnection());
	
	public MyWindow()
	{
		center();
		setIcon(FontAwesome.SEARCH_PLUS);
		setCaption(" Window");
		setHeight("50%");
		setWidth("40%");
		setPositionX(70);
		setPositionY(100);
		setImmediate(true);
		setDraggable(true);
		setResizable(false);
		
		FormLayout form = new FormLayout();
		setContent(form);
		
		final Table table = new Table();
		table.setPageLength(10);
		table.setSelectable(true);
		
		table.addContainerProperty("Coop #", String.class, null);
		table.addContainerProperty("Coop Name", String.class, null);
		table.addContainerProperty("Branch", String.class, null);
		table.addContainerProperty("Business Type", String.class, null);
		
		ResultSet select = query.statement(CoopInfo.selectCoopInfo());
		try {
			while(select.next())
			{
				table.addItem(new Object[]{ select.getString("coop_id"), select.getString("Name"), select.getString("Branch"), select.getString("BusinessType")}, new Integer(select.getInt("CoopNum")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		table.addItemClickListener(new ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
				// TODO Auto-generated method stub
				ResultSet info = query.statement(
						CoopInfo.selectCoopInfoId()
						, new String[]{""+event.getItemId()}
					);
				try {
					while(info.next()){
						
//						CoopinfosystemUI.info.setCoopName((String) event.getItem().getItemProperty("CoopNum").getValue());
//						CoopinfosystemUI.info.setCoopNum((String) event.getItem().getItemProperty("coopcode").getValue());
//						CoopinfosystemUI.info.setAcronm((String) event.getItem().getItemProperty("Acronym").getValue());
//						CoopinfosystemUI.info.setBranch((String) event.getItem().getItemProperty("Branch").getValue());	
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(event.isDoubleClick())
				{
					close();
				}
			}
		});
		
		TextField search = new TextField();
		search.setInputPrompt("Search Here ......");
		search.addTextChangeListener(new TextChangeListener() {
			
			@Override
			public void textChange(TextChangeEvent event) {
				// TODO Auto-generated method stub
				table.removeAllItems();
				
				ResultSet searchCoop = query.statement(CoopInfo.searchCoopInfo(event.getText()));
				try {
					while(searchCoop.next())
					{
						
						table.addItem(new Object[]{ searchCoop.getString("coop_id"), searchCoop.getString("Name"), searchCoop.getString("Branch"), searchCoop.getString("BusinessType")}, new Integer(searchCoop.getInt("CoopNum")));
						
					}
					searchCoop.close();					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		form.addComponent(search);
		form.addComponent(table);
		
	}

}
