package com.example.ui;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.controller.LocationListener;
import com.example.model.GroupClient;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class AddressUI extends VerticalLayout
{

	DBQuery query = new DBQuery(DBConnection.getConnection());
	private ResultSet result = null;
	
	private HorizontalLayout subLayout;
	TextArea address = new TextArea();
	TextField barangay = new TextField();
	Button searchAddress = new Button();
	ComboBox province = new ComboBox();
	ComboBox zipcode = new ComboBox();
	ComboBox city = new ComboBox();
	TextField region = new TextField();
	TextField area = new TextField();
	TextArea remarks = new TextArea();
	
	public AddressUI ()
	{
		
		setStyleName("address-layout");
		setSpacing(true);
		
		addComponent(address);
		address.setCaption("Address / Street");
		address.setStyleName("address");
		address.setColumns(24);
		address.setRows(3);
		
		addComponent(barangay);
		barangay.setCaption("Barangay");
		barangay.setStyleName("barangay");
		
		addComponent(searchAddress);
		searchAddress.setCaption("Search location");
		searchAddress.setIcon(FontAwesome.SEARCH);
		searchAddress.setStyleName("search-address");
		
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		subLayout.setWidth("100%");
		
		subLayout.addComponent(province);
		province.setCaption("Province");
		province.setStyleName("province");
		province.setTextInputAllowed(true);
		province.setNewItemsAllowed(true);
		this.setProvinceData();
		
		subLayout.addComponent(zipcode);
		zipcode.setCaption("Zip Code");
		zipcode.setStyleName("zipcode");
		zipcode.setTextInputAllowed(true);
		zipcode.setNewItemsAllowed(true);
		zipcode.addValueChangeListener(new LocationListener());
		zipcode.setId("zipcode");
		this.setZipCodeData();
		
		addComponent(city);
		city.setCaption("City");
		city.setStyleName("city");
		city.setTextInputAllowed(true);
		city.setNewItemsAllowed(true);
		city.addValueChangeListener(new LocationListener());
		city.setId("city");
		this.setCityData();
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		subLayout.setWidth("100%");
		
		subLayout.addComponent(region);
		region.setCaption("Region");
		region.setStyleName("region");
		
		subLayout.addComponent(area);
		area.setCaption("Area");
		area.setStyleName("area");
		
		addComponent(remarks);
		remarks.setCaption("Remarks");
		remarks.setStyleName("remarks");
		remarks.setColumns(24);
		remarks.setRows(3);

	}
	
	private void setProvinceData ()
	{
		result = query.statement(GroupClient.getColumnByGroup("Province", "Province"));
		try
		{
			while(result.next())
			{
				province.addItem(result.getString("Province"));
			}
			result.close();
		} catch ( SQLException e )
		{
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
	}
	
	private void setZipCodeData ()
	{
		result = query.statement(GroupClient.getColumnByGroup("ZipCode", "ZipCode"));
		try
		{
			while(result.next())
			{
				zipcode.addItem(result.getString("ZipCOde"));
			}
			result.close();
		} catch ( SQLException e )
		{
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
	}
	
	
	private void setCityData ()
	{
		result = query.statement(GroupClient.getColumnByGroup("MunOrCity, ZipCode", "MunOrCity"));
		try
		{
			while(result.next())
			{
				city.addItem(result.getString("ZipCode"));
				city.setItemCaption(result.getString("ZipCode"), result.getString("MunOrCity"));
			}
			result.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
	}

	public String getAddress() {
		return (String) address.getValue();
	}

	public String getBarangay() {
		return (String) barangay.getValue();
	}

	public String getProvince() {
		return (String) province.getValue();
	}

	public String getZipcode() {
		return (String) zipcode.getValue();
	}

	public String getCity() {
		return (String) city.getValue();
	}
	
	public String getCityCaption ()
	{
		return city.getItemCaption(city.getValue());
	}

	public String getRegion() {
		return (String) region.getValue();
	}

	public String getArea() {
		return (String) area.getValue();
	}

	public String getRemarks() {
		return (String) remarks.getValue();
	}

	public void setAddress(String address) {
		this.address.setValue(address);
	}

	public void setBarangay(String barangay) {
		this.barangay.setValue(barangay);
	}

	public void setProvince(String province) {
		this.province.setValue(province);
	}

	public void setZipcode(String zipcode) {
		this.zipcode.setValue(zipcode);
	}

	public void setCity(String city) {
		this.city.setValue(city);
	}

	public void setRegion(String region) {
		this.region.setValue(region);
	}

	public void setArea(String area) {
		this.area.setValue(area);
	}

	public void setRemarks(String remarks) {
		this.remarks.setValue(remarks);
	}
	
	public void clear ()
	{
		
		this.address.setValue("");
		this.region.setValue("");
		this.area.clear();
		this.remarks.setValue("");
		this.barangay.clear();
		this.province.clear();
		this.city.clear();
		this.zipcode.clear();
	}
}
