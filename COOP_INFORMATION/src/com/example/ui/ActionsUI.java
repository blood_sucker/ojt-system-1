package com.example.ui;

import com.example.controller.CancelListener;
import com.example.controller.SaveListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class ActionsUI extends CssLayout {
	
	private static Button save = new Button();
	private static Button cancel = new Button();
	private static Button search = new Button();

	public ActionsUI ()
	{
		
		setStyleName("actions");
		
		addComponent(search);
		search.setCaption("Search");
		search.setWidth("100%");
		search.setIcon(FontAwesome.SEARCH);
		search.setStyleName(ValoTheme.BUTTON_PRIMARY);
		search.setIcon(FontAwesome.SEARCH);
		
		addComponent(save);
		save.setCaption("Save");
		save.setWidth("100%");
		save.setIcon(FontAwesome.SAVE);
		save.setStyleName(ValoTheme.BUTTON_FRIENDLY);
		save.addStyleName("save-button");
		save.addClickListener(new SaveListener());
		save.setId("save");
		
		
		addComponent(cancel);
		cancel.setCaption("Cancel");
		cancel.setIcon(FontAwesome.REPEAT);
		cancel.setStyleName(ValoTheme.BUTTON_DANGER);
		cancel.addStyleName("cancel-button");
		cancel.addClickListener(new CancelListener());
		
		
		Button reloadPage = new Button("Reload");
		reloadPage.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				Page.getCurrent().reload();
			}
		});
		
		addComponent(reloadPage);
		reloadPage.setWidth("100%");
		
		search.addClickListener(new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				UI.getCurrent().addWindow(new SearchWindow());
			}
		});
	}

	public static String getSaveId() {
		return save.getId();
	}

	public static String getCancelId() {
		return cancel.getId();
	}

	public static void setSaveId(String save) {
		ActionsUI.save.setId(save);
	}

	public static void setCancelId(String cancel) {
		ActionsUI.cancel.setId(cancel);
	}
	
	public static void clickCancel ()
	{
		ActionsUI.cancel.click();
	}
	
	public static void setSaveCaption (String caption)
	{
		ActionsUI.save.setCaption(caption);
	}
	

}
