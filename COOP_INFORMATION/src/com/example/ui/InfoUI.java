package com.example.ui;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.example.controller.DBConnection;
import com.example.controller.DBQuery;
import com.example.model.GroupCategory;
import com.example.model.GroupClient;
import com.vaadin.data.Property.ReadOnlyException;
import com.vaadin.data.util.converter.Converter.ConversionException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class InfoUI extends VerticalLayout
{
	
	DBQuery query = new DBQuery(DBConnection.getConnection());
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	
	HorizontalLayout subLayout;
	TextField coopName = new TextField();
	TextField coopNum = new TextField();
	CheckBox main = new CheckBox();
	TextField acronm = new TextField();
	ComboBox branch = new ComboBox();
	ComboBox business = new ComboBox();
	ComboBox category = new ComboBox();
	ComboBox group = new ComboBox();
	TextField tin = new TextField();
	TextField emailCont = new TextField();
	TextField coopCode = new TextField();
	TextField cdaNum = new TextField();
	DateField dateReg = new DateField();
	DateField dateStart = new DateField();
	
	public InfoUI ()
	{
		
		setStyleName("coop-info-layout");
		setSpacing(true);
		
		addComponent(coopName);
		coopName.setSizeUndefined();
		coopName.setCaption("Coop name");
		coopName.setStyleName("coop-name");
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSizeFull();
		subLayout.setSpacing(true);
		
		subLayout.addComponent(coopNum);
		coopNum.setCaption("Coop #");
		coopNum.setStyleName("coop-number");
		
		subLayout.addComponent(main);
		subLayout.setComponentAlignment(main, Alignment.BOTTOM_RIGHT);
		main.setCaption("Main");
		main.setStyleName("main");
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		
		subLayout.addComponent(acronm);
		acronm.setCaption("Acronym");
		acronm.setSizeFull();
		acronm.setStyleName("acronym");
		
		subLayout.addComponent(branch);
		branch.setCaption("Branch");
		branch.setStyleName("branch");
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		subLayout.setWidth("100%");
		
		subLayout.addComponent(business);
		business.setCaption("Business type");
		business.setStyleName("business-type");
		
		subLayout.addComponent(category);
		category.setCaption("Category");
		category.setStyleName("category");
		
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		subLayout.setWidth("100%");
		
		subLayout.addComponent(group);
		group.setCaption("Group of");
		group.setStyleName("group");
		
		subLayout.addComponent(tin);
		subLayout.setExpandRatio(tin, 1);
		tin.setCaption("Tin");
		tin.setStyleName("tin");
		
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		subLayout.setWidth("100%");
		
		
		subLayout.addComponent(emailCont);
		emailCont.setCaption("Email / Cont. #");
		emailCont.setStyleName("email-cont");
		
		
		subLayout.addComponent(coopCode);
		coopCode.setCaption("Coop Code");
		coopCode.setStyleName("coop-code");
		
		addComponent(cdaNum);
		cdaNum.setCaption("CDA Reg #");
		cdaNum.setStyleName("cda-number");
		
		subLayout = new HorizontalLayout();
		addComponent(subLayout);
		subLayout.setSpacing(true);
		subLayout.setWidth("100%");
		
		subLayout.addComponent(dateReg);
		dateReg.setCaption("Date Registered");
		dateReg.setStyleName("date-registed");
		
		subLayout.addComponent(dateStart);
		dateStart.setCaption("Date Started");
		dateStart.setStyleName("date-started");
		
		
		ResultSet select = query.statement(GroupClient.getColumnByGroup("BusinessType", "BusinessType"));
		try {
			while(select.next()){
				business.addItem(select.getString("BusinessType"));
				
			}
			select.close();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeStatement();
		}
		
		ResultSet listBranch = query.statement(GroupClient.getColumnByGroup("Branch", "Branch"));
		try
		{
			while(listBranch.next())
			{
				branch.addItem(listBranch.getString("Branch"));
			}
			listBranch.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally{
			query.closeStatement();
			query.closeResult();
		}
		
		ResultSet selGroup = query.statement(GroupCategory.getGroup());
		try {
			while(selGroup.next())
			{
				
				group.addItem(selGroup.getString("Group_id"));
				group.setItemCaption(selGroup.getString("Group_id"), selGroup.getString("Description"));
			}
			selGroup.close();
		} catch (UnsupportedOperationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
		
		
		ResultSet listCategory = query.statement(GroupCategory.getGroup());
		try
		{
			while(listCategory.next())
			{
				category.addItem(listCategory.getString("Group_id"));
				category.setItemCaption(listCategory.getString("Group_id"), listCategory.getString("Description"));
			}
			listCategory.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		} finally {
			query.closeStatement();
			query.closeResult();
		}
	}
	
	

	public String getCoopName() {
		return (String) coopName.getValue();
	}

	public void setCoopName(String coopName) {
		this.coopName.setValue(coopName);
	}

	public String getCoopNum() {
		return coopNum.getValue();
	}

	public void setCoopNum(String coopNum) {
		this.coopNum.setValue(coopNum);
	}

	public boolean getMain() {
		return main.getValue();
	}

	public void setMain(boolean main) {
		this.main.setValue(main);
	}

	public String getAcronm() {
		return (String) acronm.getValue();
	}

	public void setAcronm(String acronm) {
		this.acronm.setValue(acronm);
	}

	public String getBranch() {
		return (String) branch.getValue();
	}

	public void setBranch(String branch) {
		this.branch.setValue(branch);
	}

	public String getBusiness() {
		return (String) business.getValue();
	}

	public void setBusiness(String business) {
		this.business.setValue(business);
	}

	public String getCategory() {
		return (String) category.getValue();
	}

	public void setCategory(String category) {
		this.category.setValue(category);
	}

	public String getGroup() {
		return (String) group.getValue();
	}

	public void setGroup(String group) {
		this.group.setValue(group);
	}

	public String getTin() {
		return (String) tin.getValue();
	}

	public void setTin(String tin) {
		this.tin.setValue(tin);
	}

	public String getEmailCont() {
		return (String) emailCont.getValue();
	}

	public void setEmailCont(String emailCont) {
		this.emailCont.setValue(emailCont);;
	}

	public String getCoopCode() {
		return (String) coopCode.getValue();
	}

	public void setCoopCode(String coopCode) {
		this.coopCode.setValue(coopCode);
	}

	public String getCdaNum() {
		return (String) cdaNum.getValue();
	}

	public void setCdaNum(String cdaNum) {
		this.cdaNum.setValue(cdaNum);
	}

	public Date getDateReg() {
		return dateReg.getValue();
	}

	public void setDateReg(String dateReg) {
		
		if( dateReg != null )
		{
			try {
				this.dateReg.setValue(formatter.parse(dateReg));
			} catch (ReadOnlyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ConversionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Date getDateStart() {
		return dateStart.getValue();
	}

	public void setDateStart(String date) {
		
		if( date != null )
		{
			try {
				this.dateStart.setValue(formatter.parse(date));
			} catch (ReadOnlyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ConversionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void clear ()
	{
		this.acronm.clear();
		this.branch.setValue(null);
		this.business.setValue(null);
		this.category.setValue(null);
		this.cdaNum.clear();
		this.coopCode.clear();
		this.coopName.clear();
		this.coopNum.clear();
		this.dateReg.clear();
		this.dateStart.clear();
		this.emailCont.clear();
		this.group.setValue(null);
		this.main.setValue(false);
		this.tin.clear();
	}
}
